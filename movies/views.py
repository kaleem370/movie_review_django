from rest_framework import filters, generics, viewsets

from .models import Movie, Trending
from .serializers import MovieSerializer, TrendingSerializer

# Create your views here.

class MovieViewSet(viewsets.ModelViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer

    # def patch(self, request, *args, **kwargs):
    #     return self.partial_update(request, *args, **kwargs)

class TrendingViewSet(viewsets.ModelViewSet):
    queryset = Trending.objects.all()
    serializer_class = TrendingSerializer

class SearchMovies(generics.ListCreateAPIView):
    search_fields = ['name']
    filter_backends = (filters.SearchFilter,)
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
