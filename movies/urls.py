from django.conf.urls import url
from rest_framework import routers
from movies.views import MovieViewSet,TrendingViewSet

router = routers.DefaultRouter()
router.register(r'movie', MovieViewSet)
router.register(r'trending_movie',TrendingViewSet)

urlpatterns = router.urls
