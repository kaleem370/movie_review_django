from django.db import models

# Create your models here.
class Movie(models.Model):

    name = models.CharField(max_length=200)
    rating = models.IntegerField()
    detail = models.TextField()
    isBookmark = models.BooleanField()
    image = models.URLField()

class Trending(models.Model):
    name = models.CharField(max_length=200)
    rating = models.IntegerField()
    detail = models.TextField()
    isBookmark = models.BooleanField()
    image = models.URLField()    



