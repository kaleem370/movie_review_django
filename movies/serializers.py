from rest_framework import serializers
from .models import Movie, Trending

class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ['id','name','rating','detail','isBookmark','image']
    

class TrendingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trending
        fields = ['id','name','rating','detail','isBookmark','image']

class SearchMoviesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = '__all__'