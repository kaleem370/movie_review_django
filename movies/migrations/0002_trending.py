# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2019-08-17 07:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Trending',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('rating', models.IntegerField()),
                ('detail', models.TextField()),
                ('isBookmark', models.BooleanField()),
                ('image', models.URLField()),
            ],
        ),
    ]
